# 红外热成像相机

## 案例展示

&emsp;&emsp;一个8*8像素的红外热成像相机，探测外界热量数据矩阵，并将其以图像的方式呈现出来。

![](/AMG8833/img/Snipaste_2021-07-14_22-47-03.jpg)

&emsp;&emsp;后续将加入双线性插值算法，让成像有更高的分辨率。

## 物理连接

### 传感器选择

&emsp;&emsp; 传感器选择如下图所示的型号为AMG8833的8*8像素红外热像仪传感器模组。

![](/AMG8833/img/Snipaste_2021-07-14_21-51-11.jpg)

### 传感器接线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

|Waffle Nano|传感器|
|---|---|
|3.3|3vo|
|IO0|SDA|
|IO1|SCL|
|GND|GND|


## 传感器库使用

&emsp;&emsp;可以获取[amg8833.py](/AMG8833/code/amg8833.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```python
from amg8833 import AMG8833 
```

&emsp;&emsp;在对象构造函数中，我们需要传入一个已经构造好的`IIC`对象

```python
# 此处省略IIC对象的构造过程
ircamera=AMG8833(i2c) #构造红外摄像机对象
```

&emsp;&emsp;我们使用红外照相机对象的`read()`方法读取出一个长度为128的一维数组温度浮点数数组。

```python
IRdata=ircamera.read() #从红外照相机中获取数据
```
&emsp;&emsp;关于此库相关细节说明详见代码注释

## 案例代码复现

&emsp;&emsp;可以获取[main.py](/AMG8833/code/main.py)函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例。

&emsp;&emsp;案例相关细节说明详见代码注释