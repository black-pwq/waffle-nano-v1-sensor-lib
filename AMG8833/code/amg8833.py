from machine import I2C,Pin
import utime
class AMG8833:
    def __init__(self,i2c):
        self.i2c = i2c
        self.addr=105 #设置需要通信的从机（AMG8833传感器）地址为105
        utime.sleep_ms(100)
    def read(self):
        int_data=[0]*64 #存放处理完成的数据
        bin_data = [0]*128 #存放原始数据
        bin_data=list(self.i2c.readfrom_mem(self.addr,0x80,128)) #从self.addr地址的0x80寄存器开始读取128个寄存器数据
        for i in range (64): #数据加工
            low=bin_data[i*2]
            hi=bin_data[i*2+1]
            int_data[i]=(hi*256+low)*0.25
        return int_data