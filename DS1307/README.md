# DS1307 钟表模型

## 案例展示





<img src="https://gitee.com/black-pwq/waffle-nano-v1-sensor-lib/raw/master/DS1307/img/ds1307_figure.jpg" alt="ds1307" style="zoom: 33%;" />

能够显示当前的年、月、日与时间，同时能够重新设置时间一校准。

## 传感器

DS1307使用I2C协议。

各寄存器地址如下：

![Timekeeper_Registers](https://gitee.com/black-pwq/waffle-nano-v1-sensor-lib/raw/master/DS1307/img/Timekeeper_Registers.png)


## 物理连线

Tiny RTC DS1307 与Waffle nano的连线选择左侧引脚较少一侧，只需与主板连四根线即可。

- [ ] SCL --> G01
- [ ] SDA --> G00
- [ ] VCC --> 3V3
- [ ] GND --> GND

## 传感器库

传感器库名为ds1307, 提供：

- 查询时间 get_time()
- 设置时间 set_time()
- 查询状态 query_halt()
- 设置状态 set_halt()

使用方法:

```python
from machine import I2C, Pin
import ds1307

i2c = I2C(1, sda = Pin(0), scl = Pin(1), freq = 100000)
ds = ds1307.DS1307(i2c) # 传入接口
ds.set_halt(False) # 启动计时
now = (year, month, day, date, hour, minute, second, subsecond) # 写入当前时间
ds.set_time(now) #设置时间
print(ds.get_time()) # 查询时间
```

## 测试样例：

```python
>>> from machine import I2C, Pin
>>> import ds1307
>>> i2c = I2C(1, sda = Pin(0), scl = Pin(1), freq = 100000)
>>> ds = ds1307.DS1307(i2c)
>>> i2c.scan()					# 查询接口地址
[80, 104]
>>> ds.query_halt() 			# 查询时钟是否暂停
False							# 时钟未暂停
>>> ds.get_time() 				# 查询时间
(2021, 7, 16, 5, 20, 54, 7, 0)  # 当前时间
>>> ds.get_time() 				# 等待 8s
(2021, 7, 16, 5, 20, 54, 16, 0) 
>>> ds.get_time()				# 等待 10s
(2021, 7, 16, 5, 20, 54, 26, 0)
>>> ds.set_halt(True)			# 设置时钟暂停计时
>>> ds.get_time()				# 获取时间
(2021, 7, 16, 5, 20, 54, 43, 0) 
>>> ds.get_time()				# 等待 5s
(2021, 7, 16, 5, 20, 54, 43, 0) # 时间未变
>>> ds.set_halt(False)			# 设置时钟开启计时
>>> ds.get_time()				# 获取时间
(2021, 7, 16, 5, 20, 54, 49, 0)
>>> ds.get_time()				#等待 5S
(2021, 7, 16, 5, 20, 54, 54, 0)
```

## 案例代码复现

可以获取[main.py](https://gitee.com/black-pwq/waffle-nano-v1-sensor-lib/blob/master/DS1307/code/main.py)函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例。

