from machine import ADC,Pin
import utime
x=ADC(Pin(5))
y=ADC(Pin(12))
z=ADC(Pin(13))
x.equ(ADC.EQU_MODEL_8)
x.atten(ADC.CUR_BAIS_DEFAULT)
y.equ(ADC.EQU_MODEL_8)
y.atten(ADC.CUR_BAIS_DEFAULT)
z.equ(ADC.EQU_MODEL_8)
z.atten(ADC.CUR_BAIS_DEFAULT)

def xyzread(x,y,z):
    accx=x.read()
    accy=y.read()
    accz=z.read()
    acclist=[accx,accy,accz]
    return acclist
steps = 0


while 1:
    acclist=xyzread(x,y,z)
    acc = acclist[0]*acclist[0]+acclist[1]*acclist[1]+acclist[2]*acclist[2]
    if (acc>2500000):
        steps=steps+1
        print(steps)
    utime.sleep(1.4)